const http = require('http');
const fs = require('fs');

const onRequest = (request, response) => {
    // *Read File HTML
    // response.writeHead(200, {"Content-Type" : "text/html"});
    // fs.readFile("./index.html", null, (err, data) => {
    //     if (err) {
    //         response.writeHead(404);
    //         response.write("File Not Found");
    //     }
    //     else {
    //         response.write(data);
    //     }
    //     response.end();
    // });

    // *JSON
    // console.log(request.method);
    // console.log(request.url);
    // response.writeHead(200, {"Content-Type" : "application/json"});
    // const data = {
    //     name: "Yudi Krisnandi",
    //     age: 22
    // };
    // response.end(JSON.stringify(data));
    // response.end(JSON.parse(data));

    // *Read File JSON
    // response.writeHead(200, {"Content-Type" : "application/json"});
    // fs.readFile("./masterdata.json", null, (err, data) => {
    //     if (err) {
    //         response.writeHead(404);
    //         response.write("File Not Found");
    //     }
    //     else {
    //         response.write(data);
    //     }
    //     response.end();
    // });
    
    
    
    
    
    // 
    console.log(request.url, request.method);
    let path = "./views/";
    switch (request.url){
        case '/':
            path += "index.html";
            response.statusCode = 200;
            break;
        case '/about':
            path += "about.html";
            response.statusCode = 200;
            break;
        case '/about-me':
            response.statusCode = 301;
            response.setHeader("Location","/about");
            return response.end();
            break;
        default :
            path += "404.html";
            response.statusCode = 404;
            break;
    }
    // * if ganti ke switch ^^^
    // if(request.url === "/"){
    //     path += "index.html";
    //     response.statusCode = 200;
    // }
    // else if(request.url === "/about"){
    //     path += "about.html";
    //     response.statusCode = 200;
    // }
    // else if(request.url === "/about-me"){
    //     response.statusCode = 301;
    //     response.setHeader('Location', '/about');
    //     return response.end();
    // }
    // else{
    //     path += "/404.html";
    //     response.statusCode = 404;
    // }

    // set header content type
    response.setHeader("Content-Type" , "text/html");
    // parameter null adalah encoding
    fs.readFile(path, null, (err, data) => {
        if (err) {
            response.writeHead(404);
            response.write("File Not Found");
        }
        else {
            response.write(data);
        }
        response.end();

    });
}

// http.createServer(onRequest).listen(8000); // buat request http server sederhana
// * Buat request http server dengan deskripsi fungsi callback
http.createServer(onRequest).listen(8000,'localhost',() => {
    console.log('Listening for request to port 8000');
});